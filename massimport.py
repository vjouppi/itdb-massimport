#!/usr/bin/python -tt
#Copyright (C) 2013, Ville Jouppi <jope@jope.fi>
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright owner nor the names of contributors to this software may be used to endorse or promote products derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import sqlite3
from sys import exit
import argparse

parser = argparse.ArgumentParser(description='Mass import items into ITDB')
parser.add_argument('serials', metavar='SERIAL', nargs='+', help='Serial number')
parser.add_argument('-d', '--db', metavar='DB', action='store', help='Fully qualified path+filename to the ITDB sqlite database (req)', required=True)
parser.add_argument('-a', '--agent', metavar='AGENT', action='store', help='Manufacturer name (req)', required=True)
parser.add_argument('-m', '--model', metavar='MODEL', action='store', help='Text for the model field (req)', required=True)
parser.add_argument('-n', '--note', metavar='NOTE', action='store', help='Text for the note field', default='')
parser.add_argument('-l', '--label', metavar='LABEL', action='store', help='Text for the label field', default='')
parser.add_argument('-u', '--user', metavar='USER', action='store', help='ITDB user to own the devices (default is nobody)', default='nobody')
parser.add_argument('-t', '--type', metavar='TYPE', action='store', help='Item type (default is phone)', default='Mobile phone')
parser.add_argument('-i', '--inuse', action ='store_true', help='Item is in use (default is stored)')

args=parser.parse_args()

s=sqlite3.connect(args.db)

want_version=4
dbversions=s.execute("select dbversion from settings")
dbversion=dbversions.fetchone()
if not dbversion:
  print "Error getting db version"
  exit (1)
elif dbversion[0] != want_version:
  print "Wrong db version, this is for version " + str(want_version)
  exit(1)

if args.inuse:
  status=0
else:
  status=1

users=s.execute("select id from users where username is ?",(args.user,))
user=users.fetchone()
if not user:
  print "No such user " + args.user
  exit(1)
else:
  user=user[0]

itemtypes=s.execute("select id from itemtypes where typedesc is ?",(args.type,))
itemtype=itemtypes.fetchone()
if not itemtype:
  print "No such itemtype " + args.type
  exit(1)
else:
  itemtype=itemtype[0]

agents=s.execute("select id from agents where title is ?",(args.agent,))
agent=agents.fetchone()
if not agent:
  print "No such agent " + args.agent
  exit(1)
else:
  agent=agent[0]

insert=[]
for serial in args.serials:
  insert.append((itemtype, serial, args.model, agent, status, user, args.note, args.label))
s.executemany("insert into items (itemtypeid,sn,model,manufacturerid,ispart,rackmountable,status,userid,comments,sn2,sn3,label) values (?,?,?,?,0,0,?,?,?,'','',?)", insert)
s.commit()

